# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
CoderslabWorkshopI::Application.config.secret_key_base = 'b0b59b0fb9859a8057efa6b30921bee3e2d9e808ed2b13e9df9fe67be6f8af7a1fc0a1c914de5389e680c4601dd1c7f07fe47702ca7e6566b6a60ed250dba2d6'
