class EventsController < ApplicationController
  before_action :set_event, only: [:edit, :update, :destroy]
  before_filter :authenticate_user!, :only => [:new, :edit, :update, :delete, :my]

  def my
    @events = current_user.adminable_events
    render :index
  end


  def index
    @events = Event.all
  end

  def show
    @event = Event.find(params[:id])
  end

  def new
    @event = Event.new
  end

  def edit
    @event = current_user.adminable_events.find(params[:id])
  end

  def create
    @event = current_user.adminable_events.new(event_params)
    @event.facebook_attenders_ids = params[:event][:facebook_attenders_ids]

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render action: 'show', status: :created, location: @event }
      else
        format.html { render action: 'new' }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url }
      format.json { head :no_content }
    end
  end

  private

    def set_event
      @event = current_user.adminable_events.find(params[:id])
    end

    def event_params
      params.require(:event).permit(:name, :description, :start_time, :end_time, :location,:owner_id)
    end



end
